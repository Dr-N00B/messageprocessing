package log

import (
	"MessagingSystem/config"
	"context"
	"os"
	"path/filepath"
	"time"

	"github.com/romana/rlog"
)

func InitLogger(ctx context.Context) {
	// Enable logging
	conf := config.GetConfig()

	logPath := conf["LOG_PATH"]
	logLevel := conf["LOG_LEVEL"]
	logFile := logPath + "/messaging_" + time.Now().Format("02012006") + ".log"
	logFile = filepath.FromSlash(logFile)
	logFile, _ = filepath.Abs(logFile)

	os.Setenv("RLOG_LOG_LEVEL", logLevel)
	rlog.UpdateEnv()

	logFileFp, err := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)

	// if unable to open file for logging, no need to rotate the file as well
	if err != nil {
		rlog.Critical("Unable to open log file. All messages will be redirected to stderr")
		return
	}

	// rlog.EnableModule(stdout.NewStdoutLogger(true))

	rlog.SetOutput(logFileFp)
	os.Setenv("RLOG_LOG_FILE", logFileFp.Name())
	os.Setenv("RLOG_LOG_STREAM", "none")
	rlog.UpdateEnv()

	rlog.Debugf("Logger initialized with level : %s", logLevel)
	go rotateLogFile(ctx, logPath)
}

func rotateLogFile(ctx context.Context, logPath string) {
	interval := time.NewTicker(24 * time.Hour)
	for {
		select {
		case <-interval.C:
			logFile := logPath + "/messaging_" + time.Now().Format("02012006") + ".log"
			logFile = filepath.FromSlash(logFile)
			logFileFp, err := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
			if err == nil {
				rlog.Infof("About to change log output. Check %s.", logFileFp.Name())
				rlog.SetOutput(logFileFp)
				os.Setenv("RLOG_LOG_FILE", logFileFp.Name())
				rlog.UpdateEnv()
			}
		case <-ctx.Done():
			return
		}
	}
}
