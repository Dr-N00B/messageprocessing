```
cat <<EOF > /etc/systemd/system/bras-messaging-system.service 
[Unit]
Description=bras messaging system process
After=network.target auditd.service rc-local.service
ConditionFileNotEmpty=/controller/MessagingSystem

# Limit of number of starts
StartLimitBurst=5


[Service]
Type=simple
WorkingDirectory=/controller
ExecStart=/controller/MessagingSystem
KillMode=process
#RestartPreventExitStatus=1 6 255 SIGABRT
#Type=notify

# make sure to restart if the process stops
Restart=on-failure
# time til it will try to restart
RestartSec=5s


[Install]
WantedBy=multi-user.target
EOF
```
