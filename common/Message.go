package common

import (
	"MessagingSystem/config"
	"strconv"
	"time"
)

type ChannelType int

// Although present, Only Email and Telegram are supported for now
const (
	Email ChannelType = iota
	Telegram
	Whatsapp
	Sms
	WebHook
)

type Message struct {
	Message string `json:"message,omitempty"`
	Channel struct {
		Telegram bool `json:"telegram,omitempty"`
		Email    bool `json:"email"`
		Whatsapp bool `json:"whatsapp,omitempty"`
		Sms      bool `json:"sms,omitempty"`
		WebHook  bool `json:"webhook,omitempty"`
	} `json:"channels"`
}

type MessageInfo struct {
	Msg            string               `json:"Message"`
	Channels       map[ChannelType]bool `json:"Channels"`
	Expiry         int                  `json:"Expiry"`
	IsRetry        bool                 `json:"IsRetry"`
	IsFallbackSent bool                 `json:"IsFallbackSent"`
	CreatedAt      time.Time
}

func ParseMsgInfo(msg Message) *MessageInfo {
	conf := config.GetConfig()

	msgInfo := &MessageInfo{
		Msg:            msg.Message,
		Channels:       make(map[ChannelType]bool),
		IsFallbackSent: false,
		Expiry:         60,
		IsRetry:        true,
		CreatedAt:      time.Now(),
	}

	// Update conf in config validation to set default values for
	msgInfo.IsRetry, _ = strconv.ParseBool(conf["RETRY_FAILED_MSG"])

	msgInfo.Expiry, _ = strconv.Atoi(conf["MSG_RETRY_EXPIRY"])

	// Set info if only enabled
	if msg.Channel.Email {
		msgInfo.Channels[Email] = false
	}

	if msg.Channel.Telegram {
		msgInfo.Channels[Telegram] = false
	}

	return msgInfo
}
