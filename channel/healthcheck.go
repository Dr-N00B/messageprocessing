package channel

import (
	"MessagingSystem/config"
	"context"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/romana/rlog"
)

type ConnectivityStatus struct {
	downStartTime time.Time
	downTime      time.Duration
	isServerUp    bool
}

func HealthCheck(ctx context.Context, pause, resume chan struct{}) {
	rlog.Info("[Health Check] started")
	cs := newConnectivityStatus()
	conf := config.GetConfig()

	pingHost := func(host string) bool {
		start := time.Now()
		conn, err := net.DialTimeout("tcp", host, 3*time.Second)
		if err != nil {
			rlog.Warnf("[Health Check] Server %+v not reachable. Err: %+v\n", host, err)
			return false
		}
		elapsed := time.Since(start)
		rlog.Debugf("[Health Check] Server %s took : %s", host, elapsed)
		defer conn.Close()
		return true
	}

	checkHealth := func() {
		hosts := strings.Split(conf["HEALTHCHECK_HOSTS"], ",")
		isServerNowUp := false
		for _, ele := range hosts {
			if pingHost(ele) {
				isServerNowUp = true
				break
			}
		}

		//  Check if server was not connected to internet, and now connected, then notify
		if !cs.isServerUp && isServerNowUp {
			cs.downTime = time.Since(cs.downStartTime)
			resume <- struct{}{}
			cs.isServerUp = true
			rlog.Infof("[Health Check] Sever back again after down time of : %v\n", cs.downTime)

		} else if cs.isServerUp && !isServerNowUp {
			// if Server was connected and is now disconnected from internet. Pause routing
			rlog.Error("[Health Check] Server went down.")
			cs.downStartTime = time.Now()
			pause <- struct{}{}
			cs.isServerUp = false
		}
	}

	interval := DEF_HEALTHCHECK_INTERVAL
	if val, ok := conf["HEALTHCHECK_INTERVAL"]; ok {
		val, err := strconv.Atoi(val)
		if err == nil {
			interval = time.Duration(val) * time.Second
		}
	}

	ticker := time.NewTicker(interval)
	for {
		select {
		case <-ctx.Done():
			ticker.Stop()
			rlog.Info("[Health Check] finished")
			return
		case <-ticker.C:
			checkHealth()
		}
	}

}

func newConnectivityStatus() *ConnectivityStatus {
	return &ConnectivityStatus{
		downTime:      0 * time.Second,
		downStartTime: time.Now(),
		isServerUp:    true,
	}
}
