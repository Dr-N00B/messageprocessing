package channel

import (
	"MessagingSystem/config"
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/romana/rlog"
)

type TelegramClient struct {
	token        string
	chatID       int64
	rateLimit    int
	currentCnt   int
	isAuthorized bool
	client       *http.Client
	url          string
	// check for authorized thing
}

type MessageReqBody struct {
	ChatId    int64       `json:"chat_id"`
	Text      interface{} `json:"text"`
	ParseMode string      `json:"parse_mode"`
}

func NewTelegramClient(ctx context.Context) *TelegramClient {
	conf := config.GetConfig()
	var chatId int64
	isAuthorized := true
	token := conf["TELEGRAM_TOKEN"]
	_, ok := conf["TELEGRAM_CHATID"]
	if !ok {
		isAuthorized = false
		rlog.Error("No chat id found. Skipping telegram channel")
		return nil
	} else {
		chatId, _ = strconv.ParseInt(conf["TELEGRAM_CHATID"], 10, 64)
	}

	rateLimit, _ := strconv.Atoi(conf["TELEGRAM_MSG_RATE"])

	// Can check if authorized to send data

	formPostUrl := "https://api.telegram.org/bot" + token + "/sendMessage"

	return &TelegramClient{
		token:        token,
		chatID:       chatId,
		rateLimit:    rateLimit,
		currentCnt:   0,
		isAuthorized: isAuthorized,
		client:       &http.Client{Timeout: 2 * time.Second},
		url:          formPostUrl,
	}
}

func (t *TelegramClient) SendData(body interface{}) (bool, bool) {
	// check if rate limit exceeded. no futher processing needed
	if t.currentCnt > t.rateLimit {
		return false, true
	}

	msg := &MessageReqBody{Text: body, ChatId: t.chatID, ParseMode: "HTML"}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(msg)
	if err != nil {
		rlog.Error("Failed sending message %+v to Telegram channel. Err : %v", body, err)
		return false, false
	}

	res, err := t.client.Post(t.url, "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		rlog.Error("Failed sending message %+v to Telegram channel. Err : %v", body, err)
		return false, false
	}
	if res.StatusCode != http.StatusOK {
		rlog.Error("Failed sending message %+v to Telegram channel. Err : %v", body, err)
		return false, false
	}

	return true, false
}

func (t *TelegramClient) MonitorLimit(ctx context.Context) {
	routingInterval := time.NewTicker(1 * time.Minute)
	for {
		select {
		case <-routingInterval.C:
			t.rateLimit = 500
		case <-ctx.Done():
			return
		}
	}

}

// func newPostRequest(url string) *http.Request {
// 	req, err := http.NewRequest(http.MethodPost, url, nil)
// 	if err != nil {
// 		return nil
// 	}
// 	req.Header.Set("Content-Type", "application/json")
// 	return req
// }
