package channel

import (
	"MessagingSystem/common"
	"MessagingSystem/config"
	"container/list"
	"context"
	"encoding/base64"
	"encoding/json"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"sync"
	"time"

	"github.com/romana/rlog"
)

var (
	SIGNAL_CHAN_CAP          int           = 1
	CHANNEL_CAPACITY         int           = 500
	DEF_QUEUE_CAPACITY       int           = 1000
	DEF_HEALTHCHECK_INTERVAL time.Duration = 10 * time.Second
)

type Routing interface {
	SendData(msg interface{}) (bool, bool)
}

type Queue struct {
	MsgQueue *list.List
	capacity int
	size     int
	sync.RWMutex
}

type RoutingInfo struct {
	Queue
	pause           chan struct{}
	resume          chan struct{}
	notify          bool
	IncomingMsgs    chan interface{}
	supportedChan   map[common.ChannelType]Routing
	fallback        common.ChannelType
	routingInterval int
}

var DefaultQueue = &RoutingInfo{
	// Notify read from config
	Queue:           *newQueue(DEF_QUEUE_CAPACITY),
	IncomingMsgs:    make(chan interface{}, CHANNEL_CAPACITY),
	pause:           make(chan struct{}),
	notify:          true,
	resume:          make(chan struct{}),
	supportedChan:   map[common.ChannelType]Routing{},
	fallback:        common.Email,
	routingInterval: 60, // TODO :make it configurable
}

func (queue *RoutingInfo) StartRouting(ctx context.Context) chan struct{} {
	rlog.Info("Message routing from queue started")
	isShutdown := make(chan struct{})

	// Register the supported channels
	// queue.supportedChan[common.Email] = NewEmailClient(ctx)
	// queue.supportedChan[common.Telegram] = NewTelegramClient(ctx)
	queue.registerChannels(ctx)

	// Load existing Messages from Storage to Queue to schedule them again
	queue.addToQueueFromStorage()

	go HealthCheck(ctx, queue.pause, queue.resume)
	go queue.routeMsgToChannels(ctx)

	storageInterval, _ := strconv.Atoi(config.GetConfig()["PERSIST_DATA_INTERVAL"])
	storageIntervalChan := time.After(time.Duration(storageInterval) * time.Second)
	//  Add messages to queue
	go func() {
		for {
			select {
			case msg := <-queue.IncomingMsgs:
				queue.AddToQueue(msg)
			case <-storageIntervalChan:
				queue.writeDataToStorage()
			case <-ctx.Done():
				// Deallocate objects here. Persist the data for further routing
				rlog.Info("Shutting down routing from queue")
				queue.writeDataToStorage()
				close(isShutdown)
				return
			}
		}
	}()
	return isShutdown

}

func (queue *RoutingInfo) registerChannels(ctx context.Context) {
	// Register the supported channels
	// Check for nil objects
	email := NewEmailClient(ctx)
	if email != nil {
		queue.supportedChan[common.Email] = email
	}

	telegram := NewTelegramClient(ctx)
	if telegram != nil {
		queue.supportedChan[common.Telegram] = telegram
	}
}

func (queue *RoutingInfo) AddToQueue(msg interface{}) {
	queue.Queue.Lock()
	queue.Queue.MsgQueue.PushBack(msg)
	queue.Queue.Unlock()
	rlog.Debugf("Message added to queue : %+v", msg)
}

func (queue *RoutingInfo) RemoveFromQueue(msg *list.Element) {
	queue.Lock()
	queue.MsgQueue.Remove(msg)
	queue.Unlock()
	rlog.Debugf("Message removed from queue : %+v", msg.Value)
}

func (queue *RoutingInfo) CopyMessages() *[]common.MessageInfo {
	var result []common.MessageInfo
	queue.Queue.RLock()
	for e := queue.Queue.MsgQueue.Front(); e != nil; e = e.Next() {
		msg := e.Value.(*common.MessageInfo)
		result = append(result, *msg)
	}
	queue.Queue.RUnlock()
	rlog.Debugf("Messages copied from queue : %+v", result)
	return &result
}

func (queue *RoutingInfo) routeMsgToChannels(ctx context.Context) {
	rlog.Info("Routing to channels started")
	routingInterval := time.After(0 * time.Second)
	for {
		if routingInterval == nil {
			routingInterval = time.After(time.Duration(queue.routingInterval) * time.Second)
		}
		select {
		case <-ctx.Done():
			rlog.Info("Routing to channels stopped. Context expired")
			return
		case <-routingInterval:
			rlog.Info("Routing interval reached. Scheduling message delivery")
			queue.sendMessages()
			routingInterval = time.After(time.Duration(queue.routingInterval) * time.Second)

		case <-queue.pause:
			rlog.Warn("Routing messages to channels is paused")
			isDone := false
			for {
				select {
				case <-queue.resume:
					rlog.Info("Routing will resume")
					isDone = true
				case <-ctx.Done():
					rlog.Info("Routing to channels stopped. Context expired")
					return
				}
				if isDone {
					break
				}
			}
		}
	}

}

func (queue *RoutingInfo) sendMessages() {
	rlog.Info("sendMessages started")
	entriesToPurge := []*list.Element{}

	// Loop through all entries of the queue,
	for e := queue.MsgQueue.Front(); e != nil; e = e.Next() {
		msgInfo, ok := e.Value.(*common.MessageInfo)
		if !ok {
			rlog.Error("Message type is not expected.")
			continue
		}
		rlog.Infof("Message processing started for :%v", msgInfo.Msg)
		// Final status will be used to decide if message should be removed from the queue or not
		finalStatus := true

		// Can send parallely to different channels if this portion becomes bottleneck
		for k, suppObj := range queue.supportedChan {
			// check that supported channels is enabled and msg is not yet sent
			rlog.Infof("Supported channel for :%v is %v", msgInfo.Msg, reflect.TypeOf(suppObj))
			if currStatus, ok := msgInfo.Channels[k]; ok && !currStatus {
				status, rateLimitExceeded := suppObj.SendData(msgInfo.Msg)

				// check if the rate limit for that channel is exceeded, wait for limit to reset
				if rateLimitExceeded {
					rlog.Info("Rate limit exceeded")
					continue
				} else {
					// Change the finalStatus accordingly
					finalStatus = finalStatus && status
					msgInfo.Channels[k] = status

					// If the current channel is fallback, set the flags to avoid resending the
					// message in case all retries failed
					if k == queue.fallback {
						msgInfo.IsFallbackSent = true
					}
				}
			}
		}

		// If finalStatus is true means all the channels were able to send the message successfully
		// and add the message in the list to purge entries.
		// Note: Not removing the element directly as removing directly will invalidate the iterator
		if finalStatus {
			entriesToPurge = append(entriesToPurge, e)
		} else if msgInfo.IsRetry {
			// if msg sending failed for any of the channel, reset the expiry value
			msgInfo.Expiry -= queue.routingInterval

			// All message retry failed for that channel
			if msgInfo.Expiry <= 0 && !msgInfo.IsFallbackSent {
				rlog.Info("All retries failed. Sending message via fallback channel")
				// Max retries failed, send message via fallback
				status, rateLimitExceeded := queue.supportedChan[queue.fallback].SendData(msgInfo.Msg)

				// Sending data to fallback, but message couldn't be sent due to message limit is over,
				// wait for the limit to reset to send the data again
				if rateLimitExceeded {
					rlog.Debug("Fallback channel's rate limit exceeded. Wait for next scheduling")
				} else {
					// if message was failed, log the message and discard the message
					if !status {
						rlog.Warnf("Message failed to send after consuming all retries. Discarding msg : : %+v ", msgInfo)
					}
					// discard the message
					entriesToPurge = append(entriesToPurge, e)
				}

			}
		}
	}

	// Remove the successfully sent items from the queue.
	for _, val := range entriesToPurge {
		queue.RemoveFromQueue(val)
	}
	rlog.Info("sendMessages finished")
}

func (queue *RoutingInfo) writeDataToStorage() {
	rlog.Debug("Writing Messages to Storage Started")
	conf := config.GetConfig()
	if loc, ok := conf["PERSIST_DATA_LOCATION"]; ok {
		result := queue.CopyMessages()
		resJson, err := json.Marshal(result)
		if err != nil {
			rlog.Errorf("Unable to Marshal data for storage. Err : %+v", err)
			return
		}
		resBase64 := base64.StdEncoding.EncodeToString(resJson)
		if !filepath.IsAbs(loc) {
			loc, _ = filepath.Abs(loc)
		}
		err = os.WriteFile(loc, []byte(resBase64), 0755)
		if err != nil {
			rlog.Errorf("Unable to write data to file. Err : %+v", err)
		}
	}
}

func (queue *RoutingInfo) addToQueueFromStorage() {
	rlog.Debug("Reading messages from file storage")
	conf := config.GetConfig()
	if loc, ok := conf["PERSIST_DATA_LOCATION"]; ok {
		if !filepath.IsAbs(loc) {
			loc, _ = filepath.Abs(loc)
		}
		content, err := os.ReadFile(loc)
		if err != nil {
			rlog.Errorf("Unable to read data from file. Err : %+v", err)
			return
		}

		data, err := base64.StdEncoding.DecodeString(string(content))
		if err != nil {
			rlog.Errorf("Unable to decode data %+v. Err : %+v", data, err)
			return
		}

		var result []common.MessageInfo
		err = json.Unmarshal(data, &result)
		if err != nil {
			rlog.Errorf("Unable to unmarshal data %+v. Err : %+v", data, err)
			return
		}

		for _, val := range result {
			queue.AddToQueue(&val)
			rlog.Debugf("Adding message %+v to queue from storage", val)
		}

	}

}

func newQueue(cap int) *Queue {
	return &Queue{
		MsgQueue: new(list.List),
		capacity: cap,
		size:     0,
	}
}
