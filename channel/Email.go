package channel

import (
	"MessagingSystem/config"
	"bytes"
	"context"
	"fmt"
	"mime/quotedprintable"
	"net/smtp"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/romana/rlog"
)

type EmailClient struct {
	header     string
	from_email string
	to_email   []string
	auth       smtp.Auth
	smtpAdrr   string
	body       string
	rateLimit  int
	// implement mutex here for synced access to rateLimit
}

func NewEmailClient(ctx context.Context) *EmailClient {
	conf := config.GetConfig()

	header_message := ""
	from_email := conf["MAIL_FROM"]
	to_email := strings.Split(conf["MAIL_TO"], ",")
	subject := conf["MAIL_SUBJECT"]

	rateLimit, _ := strconv.Atoi(conf["MAIL_RATE"])

	header := make(map[string]string)
	header["From"] = from_email
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", "text/html")
	header["Content-Disposition"] = "inline"
	header["Content-Transfer-Encoding"] = "quoted-printable"

	for key, value := range header {
		header_message += fmt.Sprintf("%s: %s\r\n", key, value)
	}

	auth := smtp.PlainAuth(conf["MAIL_USER"], from_email, conf["MAIL_PASSWORD"], conf["MAIL_SMTP_SERVER"])
	smtpAddr := conf["MAIL_SMTP_SERVER"] + ":" + conf["MAIL_PORT"]

	return &EmailClient{
		header:     header_message,
		from_email: from_email,
		to_email:   to_email,
		auth:       auth,
		smtpAdrr:   smtpAddr,
		body:       "",
		rateLimit:  rateLimit, // Implement rate limits
	}
}

func (mail *EmailClient) SendData(body interface{}) (bool, bool) {
	d, ok := body.(string)
	if !ok {
		rlog.Warn("Only text messages supported now.")
	}

	//dStr := string(d)
	data, err := url.QueryUnescape(d)
	if err != nil {
		rlog.Errorf("Error while trying to unescape body : %+v", err)
		data = d
	}

	var body_message bytes.Buffer
	temp := quotedprintable.NewWriter(&body_message)
	temp.Write([]byte(data))
	defer temp.Close()
	message := mail.header + "\r\n" + body_message.String()
	err = smtp.SendMail(mail.smtpAdrr, mail.auth, mail.from_email, mail.to_email, []byte(message))
	if err != nil {
		rlog.Errorf("Error while sending message via email : %+v", err)
		return false, false
	}
	rlog.Info("Message sent via email successfully")
	return true, false
}

func (email *EmailClient) MonitorLimit(ctx context.Context) {
	routingInterval := time.NewTicker(24 * time.Hour)
	for {
		select {
		case <-routingInterval.C:
			rlog.Debug("Resetting email limit")
			email.rateLimit = 500
		case <-ctx.Done():
			return
		}
	}
}
