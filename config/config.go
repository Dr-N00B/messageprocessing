package config

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

// type MsgSysConf map[string]string

// Package level variables are instantiated even before main is called.
// Since this file must be loaded for Service to work properly.
// Calling this before will elimiate the lazy singleton call
var propObj map[string]string

func InitConfFile(file string) {
	propObj = loadPropertiesFile(file)
	validateConfEntries()
}

func GetConfig() map[string]string {
	return propObj
}

func loadPropertiesFile(filename string) map[string]string {
	conf := make(map[string]string)
	filename, _ = filepath.Abs(filename)
	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("loadPropertiesFile Error observed while opening the %v file. Error : %+v\n", filename, err)
		os.Exit(2)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	if err := scanner.Err(); err != nil {
		fmt.Printf("loadPropertiesFile Error observed during scanning the %v file. Error : %+v\n", filename, err)
		os.Exit(2)
	}
	for scanner.Scan() {
		data := strings.SplitN(scanner.Text(), "=", 2)
		data[0] = strings.Trim(data[0], " ")
		data[1] = strings.Trim(data[1], " ")
		conf[data[0]] = data[1]
	}
	return conf
}

func validateConfEntries() {
	if val, ok := propObj["HOST"]; !ok || len(val) == 0 {
		propObj["HOST"] = "localhost"
	}

	if val, ok := propObj["PORT"]; !ok || len(val) == 0 {
		propObj["PORT"] = "5000"
	}

	if val, ok := propObj["HEALTHCHECK_HOSTS"]; !ok || len(val) == 0 {
		propObj["HEALTHCHECK_HOSTS"] = "8.8.8.8:443,1.1.1.1:443"
	}

	if val, ok := propObj["HEALTHCHECK_INTERVAL"]; !ok || len(val) == 0 {
		propObj["HEALTHCHECK_INTERVAL"] = "10"
	}

	if val, ok := propObj["LOG_LEVEL"]; !ok || len(val) == 0 {
		propObj["LOG_LEVEL"] = "INFO"
	}

	if val, ok := propObj["ROUTING_INTERVAL"]; !ok || len(val) == 0 {
		propObj["ROUTING_INTERVAL"] = "60"
	}

	if val, ok := propObj["RETRY_FAILED_MSG"]; !ok || len(val) == 0 {
		propObj["RETRY_FAILED_MSG"] = "false"
	}

	if val, ok := propObj["MSG_RETRY_EXPIRY"]; !ok || len(val) == 0 {
		propObj["MSG_RETRY_EXPIRY"] = "0"
	}

	if val, ok := propObj["PERSIST_DATA_INTERVAL"]; !ok || len(val) == 0 {
		propObj["PERSIST_DATA_INTERVAL"] = "300"
	}

	if val, ok := propObj["LOG_PATH"]; !ok || len(val) == 0 {
		propObj["LOG_PATH"] = "/tmp"
	}

	if val, ok := propObj["PERSIST_DATA_LOCATION"]; !ok || len(val) == 0 {
		propObj["PERSIST_DATA_LOCATION"] = "./MessageSystemUnsentMessages.txt"
	}
}
