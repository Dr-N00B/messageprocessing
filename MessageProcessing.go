package main

import (
	"MessagingSystem/channel"
	"MessagingSystem/config"
	"MessagingSystem/log"
	"MessagingSystem/server"
	"context"
	"github.com/romana/rlog"
	"os"
	"path/filepath"
)

var (
	daemonDir  = filepath.Dir(os.Args[0])
	configFile = daemonDir + "/MessagingSystem.properties"
)

func main() {
	// Programs that use Contexts should follow these rules to keep interfaces consistent across packages and enable static analysis tools to check context propagation:
	//config_file := flag.String("config", "", "Config file for the service.")
	//flag.Parse()
	//if *config_file == "" {
	//	fmt.Println("No config file provided. Cannot continue. Exiting")
	//	os.Exit(2)
	//}

	config.InitConfFile(configFile)

	ctx, cancel := context.WithCancel(context.Background())

	// Init loggers. It will take care of log file rotation also
	log.InitLogger(ctx)

	rlog.Info("Message Processing system started")

	// Step : Initialize the channel package with current context
	isQueueShutdown := channel.DefaultQueue.StartRouting(ctx)

	DefaultServer := server.DefaultServer()
	idleConnsClosed := DefaultServer.StartServer(ctx, channel.DefaultQueue.IncomingMsgs)

	// stop the servers
	DefaultServer.StopServer(ctx)

	// Wait for server to shutdown
	<-idleConnsClosed
	cancel()
	<-isQueueShutdown
	rlog.Info("Message Processing system shutdown")
}
