package server

import (
	"MessagingSystem/common"
	"MessagingSystem/config"
	"context"
	"encoding/json"
	"mime"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"

	"github.com/romana/rlog"
)

type ServerInfo struct {
	server          *http.Server
	idleConnsClosed chan struct{}
}

type messageHandler struct {
	notification chan interface{}
}

// TODO : Add a logger to server
func (srvrInfo *ServerInfo) StartServer(ctx context.Context, incomingMsgs chan interface{}) chan struct{} {
	srvrInfo.idleConnsClosed = make(chan struct{})

	go func() {
		conf := config.GetConfig()
		addr := net.JoinHostPort(conf["HOST"], conf["PORT"])
		mux := http.NewServeMux()
		mux.Handle("/notify", enforceJSONEncoding(&messageHandler{notification: incomingMsgs}))
		srvrInfo.server = &http.Server{
			Addr:           addr,
			Handler:        mux,
			MaxHeaderBytes: 1 << 20,
		}
		if err := srvrInfo.server.ListenAndServe(); err != http.ErrServerClosed {
			rlog.Errorf("Listen Error : %v\n", err)
		}
	}()

	return srvrInfo.idleConnsClosed
}

func (srvrInfo *ServerInfo) StopServer(ctx context.Context) {

	go func(ctx context.Context) {

		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		signal.Notify(sigint, syscall.SIGTERM)
		<-sigint

		if srvrInfo.server != nil {
			if err := srvrInfo.server.Shutdown(ctx); err != nil {
				rlog.Errorf("HTTP server Shutdown Error: %v\n", err)
			} else {
				rlog.Info("Server Shutdown successfully")
			}
		}
		close(srvrInfo.idleConnsClosed)
	}(ctx)

}

func DefaultServer() *ServerInfo {
	return &ServerInfo{}
}

func (msgHandler *messageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rlog.Debug("Serve HTTP of msgHandler called")
	if r.URL.Path != "/notify" {
		http.NotFound(w, r)
		return
	}
	switch r.Method {
	case http.MethodPost:
		msgHandler.processMessage(w, r)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (msgHandler *messageHandler) processMessage(w http.ResponseWriter, r *http.Request) {
	var msg common.Message

	err := json.NewDecoder(r.Body).Decode(&msg)
	if err != nil {
		rlog.Errorf("Request body is malformed. Error : %+v", err)
		http.Error(w, "Request body is malformed. Error : "+err.Error(), http.StatusNotAcceptable)
		return
	}

	decodedMsg, err := url.QueryUnescape(msg.Message)

	if err != nil {
		rlog.Errorf("Message is not correctly encoded. Error : %s", err.Error())
		http.Error(w, "Either normal or URL encoded message expected. Error : "+err.Error(), http.StatusNotAcceptable)
		return
	}

	msg.Message = decodedMsg
	go func() {
		msgInfo := common.ParseMsgInfo(msg)
		msgHandler.notification <- msgInfo
	}()
	w.WriteHeader(http.StatusOK)
	rlog.Debug("Serve HTTP of msgHandler Ended")
}

func enforceJSONEncoding(next http.Handler) http.Handler {
	enforceJSON := func(w http.ResponseWriter, r *http.Request) {
		rlog.Debug("Middleware enforceJSONEncoding called")
		// Enforcing JSON body
		contentType := r.Header.Get("Content-Type")
		if contentType != "" {
			mimeType, _, err := mime.ParseMediaType(contentType)
			if err != nil {
				rlog.Errorf("Malformed Content-Type header. Error : %+v", err)
				http.Error(w, "Malformed Content-Type header", http.StatusBadRequest)
				return
			}

			if mimeType != "application/json" {
				rlog.Error("Content-Type header must be application/json.")
				http.Error(w, "Content-Type header must be application/json", http.StatusUnsupportedMediaType)
				return
			}
		}
		// Calling the actual HandleFunc
		next.ServeHTTP(w, r)
		rlog.Debug("Middleware enforceJSONEncoding ended")
	}

	return http.HandlerFunc(enforceJSON)
}
